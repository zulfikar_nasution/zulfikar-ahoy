import shapefile					#mimport modul shapefile
w=shapefile.Writer()				#deklarasikan fungsi writer
w.shapeType							#apply shapetype

w.field("kolom1","C")				#membuat kolom dengan tipe data character
w.field("kolom2","C")

w.record("ahoy","satu")				# mengisi record pada kolom yang sudah disediakan


w.poly(parts=[[[-1.5,1],[0,3.5], [1.5,1],[-1.5,1]]],shapeType=shapefile.POLYLINE)	#membuat titik koordinat yang menghasilkan bentuk segitiga sama sisi

w.save("quiz2") 	#menyimpan file dalam bentuk shp file